-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2019 at 07:12 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ingeniero_db`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteCertificado` (IN `_id` INTEGER(11))  BEGIN
     declare aux int;
     select id into aux from `h_colegiado` where id=_id;
     delete from h_certificado where id = _id;
     IF ISNULL(aux) THEN
        delete from h_miembro where id = _id;
     END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteColegiado` (IN `_id` INTEGER(11))  BEGIN
     delete from h_colegiado where id = _id;
     delete from h_certificado where id = _id;
     delete from h_miembro where id = _id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getAll` ()  BEGIN
select * from `h_slide` order by id desc;
select * from `h_informe` order by id desc;
select * from `h_noticia` order by id desc;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `spEditImagenes` (IN `spGaleria` VARCHAR(50), IN `spId_Galeria` INTEGER(11), IN `spId` INTEGER(11), IN `spAgregar` BOOLEAN)  BEGIN
if spAgregar = TRUE then
   insert into h_imagenes(galeria,id_galeria) values(spGaleria,spId_Galeria);
ELSE
    update h_imagenes SET galeria=spGaleria where id = spId;
end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `spEditMiembro` (IN `_id` INTEGER(11), IN `_matricula` VARCHAR(50), IN `_ap_Paterno` VARCHAR(200), IN `_ap_Materno` VARCHAR(200), IN `_nombre` VARCHAR(200), IN `_fecha_Nac` VARCHAR(200), IN `_incorporacion` VARCHAR(200), IN `_estado` VARCHAR(200), IN `_vigencia` VARCHAR(200), IN `_numero` INTEGER(11), IN `_fecha_caducidad` VARCHAR(200), IN `_certificacion` BOOLEAN)  BEGIN
declare aux int;
update h_miembro set matricula = _matricula, ap_Paterno = _ap_Paterno, ap_Materno = _ap_Materno, nombre= _nombre, fecha_Nac = _fecha_Nac where id = _id;
update `h_colegiado` set incorporacion = _incorporacion, estado =_estado, vigencia = _vigencia where id = _id;
select id into aux from `h_certificado` where id=_id;
IF ISNULL(aux) THEN
   IF _certificacion = true then
      insert into h_certificado(id,numero,fecha_caducidad) values (_id,_numero,_fecha_caducidad);
   END IF;
ELSE
    IF _certificacion = true THEN
       update `h_certificado` set numero =_numero, fecha_caducidad= _fecha_caducidad where id=_id;
    ELSE
    delete from h_certificado where id = _id;
    END IF;
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `spEditPonente` (IN `spNombre` VARCHAR(200), IN `spEspecialidad` VARCHAR(200), IN `spPonente` VARCHAR(50), IN `spId_Evento` INTEGER(11), IN `spId` INTEGER(11), IN `spAgregar` BOOLEAN)  BEGIN
if spAgregar = TRUE then
   insert into h_ponente(nombre,especialidad,ponente,id_evento) values(spNombre,spEspecialidad,spPonente,spId_Evento);
ELSE
    update h_ponente SET nombre = spNombre, especialidad=spEspecialidad, ponente=spPonente where id = spId;
end if;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `h_admin`
--

CREATE TABLE `h_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_admin`
--

INSERT INTO `h_admin` (`id`, `username`, `password`, `type`) VALUES
(1, 'admin', 'admin', 'diseño');

-- --------------------------------------------------------

--
-- Table structure for table `h_administrador`
--

CREATE TABLE `h_administrador` (
  `id` int(11) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `contrasena` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_administrador`
--

INSERT INTO `h_administrador` (`id`, `usuario`, `contrasena`) VALUES
(1, 'ciro', 'ciro');

-- --------------------------------------------------------

--
-- Table structure for table `h_certificado`
--

CREATE TABLE `h_certificado` (
  `id` int(11) NOT NULL,
  `numero` int(100) NOT NULL,
  `fecha_caducidad` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_certificado`
--

INSERT INTO `h_certificado` (`id`, `numero`, `fecha_caducidad`) VALUES
(1, 987654321, '2018-07-10T05:00:00.000Z'),
(5, 55555, '2018-06-07T05:00:00.000Z');

-- --------------------------------------------------------

--
-- Table structure for table `h_colegiado`
--

CREATE TABLE `h_colegiado` (
  `id` int(11) NOT NULL,
  `incorporacion` varchar(200) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `vigencia` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_colegiado`
--

INSERT INTO `h_colegiado` (`id`, `incorporacion`, `estado`, `vigencia`) VALUES
(1, '2018-05-04T05:00:00.000Z', 'INHABIL', '2018-04-24T05:00:00.000Z'),
(4, '2018-05-02T05:00:00.000Z', 'HABIL', '2018-04-24T05:00:00.000Z'),
(5, '2018-04-24T05:00:00.000Z', 'INHABIL', '2018-05-02T05:00:00.000Z'),
(6, '2018-05-02T05:00:00.000Z', 'HABIL', '2018-05-02T05:00:00.000Z'),
(7, '2018-06-03T05:00:00.000Z', 'HABIL', '2018-06-03T05:00:00.000Z');

-- --------------------------------------------------------

--
-- Table structure for table `h_decano`
--

CREATE TABLE `h_decano` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `decano` varchar(50) NOT NULL,
  `mandato` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_decano`
--

INSERT INTO `h_decano` (`id`, `nombre`, `decano`, `mandato`) VALUES
(1, 'Carlos Francisco García Garayar', 'decano0001.png', '1996 - 1998'),
(2, 'Silvestre Tacas Ruiz', 'decano0002.png', '1998 - 2001'),
(3, 'Luis Alejandro Muñante Vergara', 'decano0003.png', '2001 - 2003'),
(4, 'Silvestre Tacas Ruiz', 'decano0004.png', '2003 - 2005'),
(5, 'Edilberto Feliz Vilca Ancahante', 'decano0005.png', '2005 - 2007'),
(6, 'Alvaro Tonino Gutierrez Vela', 'decano0006.png', '2008 - 2010'),
(7, 'Victor Segovia Palomino', 'decano0007.png', '2010 - 2012'),
(8, 'Edilberto Felix Vilca Anchante', 'decano0008.png', '2012 - 2014'),
(9, 'Edilberto Felix Vilca Anchante', 'decano0009.png', '2015 - 2016');

-- --------------------------------------------------------

--
-- Table structure for table `h_enlace`
--

CREATE TABLE `h_enlace` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `enlace` varchar(50) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_enlace`
--

INSERT INTO `h_enlace` (`id`, `titulo`, `enlace`, `url`) VALUES
(1, 'Diario oficial del vicentenario El peruano', 'enlace0001.jpg', 'http://elperuano.pe/'),
(2, 'Organismo Oficial de las contrataciones del Estado', 'enlace0002.jpg', 'http://portal.osce.gob.pe/osce/'),
(3, 'La contraloria general de  la republica', 'enlace0003.jpg', 'http://www.contraloria.gob.pe/wps/wcm/connect/cgrnew/as_contraloria/as_portal');

-- --------------------------------------------------------

--
-- Table structure for table `h_evento`
--

CREATE TABLE `h_evento` (
  `id` int(11) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `lugar` varchar(200) NOT NULL,
  `fecha` varchar(100) NOT NULL,
  `hora` varchar(50) NOT NULL,
  `inscripcion` longtext NOT NULL,
  `descripcion` longtext NOT NULL,
  `cuenta` varchar(50) NOT NULL,
  `preEvento` varchar(50) NOT NULL,
  `evento` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_evento`
--

INSERT INTO `h_evento` (`id`, `titulo`, `lugar`, `fecha`, `hora`, `inscripcion`, `descripcion`, `cuenta`, `preEvento`, `evento`) VALUES
(1, 'ARTE RUPESTRE', 'Sala A \"Ricardo Valencia Menegotto\" CN-CIP', 'Mon Apr 29 2019 00:00:00 GMT-0500 (hora estándar de Perú)', '0700PM0900PM', 'Eventoscip1@cip.org.pe', '25000 años e inicio del arte\nPrincipales cuevas del arte rupestre\nLas Venus del paleolitico superior', 'cuenta0001.jpg', 'preEvento0001.PNG', 'evento0001.jpg'),
(2, 'ESTUDIA Y TRABAJA EN CANADA O AUSTRALIA', 'AUDITORIO \"MARIO SAMAME BOGGIO\" CIP-CN', 'Fri May 24 2019 00:00:00 GMT-0500 (hora estándar de Perú)', '0700PM0900PM', 'Visualiza el lado derecho de la web', 'CONFERENCIA INFORMATIVA\n\nINFORMATE SOBRE LAS OPCIONES DE ESTUDIOS Y TRABAJO, CREDITOS EDUCATIVOS Y MAS.', 'cuenta0002.jpg', 'preEvento0002.PNG', 'evento0002.jpg'),
(3, 'Congreso Nacional de Contadores Públicos del Perú', 'Huaraz', 'Wed Aug 29 2018 00:00:00 GMT-0500 (hora estándar de Perú)', '0800AM0400PM', 'Telf: 043-587603 / Cel: 951094001 / ww.congreso2018.CCPancash.org', 'La era digital y la internacionalización de los negocios, retos de la profesión contable', 'cuenta0003.jpg', 'preEvento0003.jpg', 'evento0003.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `h_galeria`
--

CREATE TABLE `h_galeria` (
  `id` int(11) NOT NULL,
  `categoria` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_galeria`
--

INSERT INTO `h_galeria` (`id`, `categoria`) VALUES
(5, 'Galeria 1'),
(6, 'Galeria 2');

-- --------------------------------------------------------

--
-- Table structure for table `h_imagenes`
--

CREATE TABLE `h_imagenes` (
  `id` int(11) NOT NULL,
  `galeria` varchar(50) NOT NULL,
  `id_galeria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_imagenes`
--

INSERT INTO `h_imagenes` (`id`, `galeria`, `id_galeria`) VALUES
(25, 'galeria0001.jpg', 5),
(26, 'galeria1001.jpg', 5),
(27, 'galeria2001.jpg', 5),
(28, 'galeria3001.jpg', 5),
(29, 'galeria4001.jpg', 5),
(30, 'galeria5001.jpg', 5),
(31, 'galeria6001.jpg', 5),
(32, 'galeria7001.jpg', 5),
(33, 'galeria8001.jpg', 5),
(34, 'galeria0034.jpg', 6),
(35, 'galeria1034.jpg', 6),
(36, 'galeria2034.jpg', 6),
(37, 'galeria3034.jpg', 6),
(38, 'galeria4034.jpg', 6),
(39, 'galeria5034.jpg', 6),
(40, 'galeria6034.jpg', 6),
(41, 'galeria7034.jpg', 6);

-- --------------------------------------------------------

--
-- Table structure for table `h_informe`
--

CREATE TABLE `h_informe` (
  `id` int(11) NOT NULL,
  `header` varchar(200) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `descripcion` longtext NOT NULL,
  `informe` varchar(50) NOT NULL,
  `nombre_archivo` varchar(100) NOT NULL,
  `fecha` varchar(500) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_informe`
--

INSERT INTO `h_informe` (`id`, `header`, `titulo`, `descripcion`, `informe`, `nombre_archivo`, `fecha`, `fecha_registro`) VALUES
(1, 'Próxima ceremonia de colegiatura', 'REQUISITOS PARA INCORPORARSE AL COLEGIO DE CONTADORES PÚBLICOS DE MADRE DE DIOS', 'Mayor Infomración y Requisitos:\r\nOficina Institucional Jr. Jaime Troncoso N° 778.\r\nccpmdd@gmail.com / RPC: 941275156', 'informe0001.pdf', 'GOREMAD - RERPRE01932014.pdf', 'Sun Jun 17 2018 00:00:00 GMT-0500 (hora estándar de Perú)', '2018-06-11 05:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `h_miembro`
--

CREATE TABLE `h_miembro` (
  `id` int(11) NOT NULL,
  `matricula` varchar(50) NOT NULL,
  `ap_Paterno` varchar(100) NOT NULL,
  `ap_Materno` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `fecha_Nac` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_miembro`
--

INSERT INTO `h_miembro` (`id`, `matricula`, `ap_Paterno`, `ap_Materno`, `nombre`, `fecha_Nac`) VALUES
(1, '11111', 'Yupanqui', 'Pumachapi', 'Ciro', '2018-05-02T05:00:00.000Z'),
(4, '44444', 'Saavedra', 'Apellido', 'Ciro', '2018-04-24T05:00:00.000Z'),
(5, '45456', 'Saavedra', 'Apellido', 'REYMER JOHAO', '2018-04-24T05:00:00.000Z'),
(6, '55555', 'Vilcas', 'Villalba', 'Denis', '2018-05-02T05:00:00.000Z'),
(7, '22222', 'Vega', 'Maceda', 'Becker', '2018-06-03T05:00:00.000Z');

-- --------------------------------------------------------

--
-- Table structure for table `h_normas_legales`
--

CREATE TABLE `h_normas_legales` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `norma` varchar(50) NOT NULL,
  `nombre_archivo` varchar(200) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_normas_legales`
--

INSERT INTO `h_normas_legales` (`id`, `titulo`, `norma`, `nombre_archivo`, `fecha`) VALUES
(1, 'D.S. Libre Ejercicio de Profesiones', 'norma0001.pdf', 'NORMA0001.pdf', '2018-07-09 05:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `h_nosotros`
--

CREATE TABLE `h_nosotros` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `descripcion` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_nosotros`
--

INSERT INTO `h_nosotros` (`id`, `titulo`, `descripcion`) VALUES
(1, 'Historia', 'El Colegio de Ingenieros del Perú nació de un planteamiento propuesto en la Primera Conferencia Nacional de Ingeniería realizada en 1932 por la Sociedad de Ingenieros del Perú. Aunque dicha propuesta no prosperó, 30 años después se cristalizó ante la necesidad de colegiación de los ingenieros.\nEl apoyo tenaz del ingeniero Enrique Martinelli Senador de la República en ese entonces y la Directiva de la Sociedad de Ingenieros del Perú hace posible que el 8 de junio de 1962 se promulgue la Ley 14086 que crea el Colegio de Ingenieros del Perú. La norma fue rubricada por el Presidente Manuel Prado y el Ingeniero Jorge Grieve, ex presidente de la Sociedad de Ingenieros del Perú en 1961.\nSin embargo, en 1986 la directiva que presidía el ingeniero Gonzalo García Núñez ante las diferentes modificaciones que tenía el estatuto, consideró necesario elaborar un nuevo estatuto acorde a la modernidad de ese entonces y gestionó una nueva ley  para  el Colegio de Ingenieros del Perú, que permita la descentralización y autonomía de los Consejos Departamentales.\nAsí el 20 de enero de 1987, el Congreso aprueba la Ley Nº 24648 derogándose la Ley 14086 que originalmente creó el Colegio de Ingenieros del Perú.'),
(3, 'Misión', 'Somos una institución deontológico, sin fines de lucro, que representa y agrupa a los ingenieros profesionales del Perú, de todas las especialidades, que cautela y preserva el comportamiento ético de sus miembros, y debe asegurar al Perú que cuenta con una profesión nacional que ejerce la ingeniería en un contexto de orden, respeto, competitividad, calidad y ética, y que está enraizada en sus valores sociales, culturales y políticos, como base fundamental en el proceso de desarrollo de la nación.'),
(4, 'Visión', 'Ser reconocida como una institución sólida, que patrocina el manejo eficiente del conocimiento, con la finalidad de orientar a la sociedad peruana en las grandes decisiones, fomentando la practica de valores y comportamiento ético de los ingenieros profesionales, así como elevando la calidad de la ingeniería, apoyando el crecimiento del país en el contexto de la globalización.');

-- --------------------------------------------------------

--
-- Table structure for table `h_noticia`
--

CREATE TABLE `h_noticia` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `descripcion` longtext NOT NULL,
  `noticia` varchar(50) NOT NULL,
  `fuente` varchar(200) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_noticia`
--

INSERT INTO `h_noticia` (`id`, `titulo`, `descripcion`, `noticia`, `fuente`, `fecha`) VALUES
(1, 'Realidad actual y perspectivas del sector de los camelidos domesticos', 'La Junta de Decanos de Colegios de Contadores Públicos del Perú aprobó el Cronograma de Eventos Nacionales para el año 2018...', 'noticia0001.jpg', 'http://www.ccpmadrededios.org.pe/', '2018-06-08 15:54:05'),
(2, 'Encuentro deportivo nacional de confraternidad cip 2019 detalle 09-04-2019', 'La Certificación y Rectificación Profesional, es una herramienta que brinda prestigio profesional y personal, así mismo acredita la experiencia y pericia del profesional contable....', 'noticia0002.jpg', 'http://www.ccpmadrededios.org.pe/', '2018-06-08 15:54:37'),
(3, 'Conferencia informativa estudia y trabaja en canada o australia detalle', 'La Junta de Decanos de Colegios de Contadores Públicos del Perú aprobó el Cronograma de Certificación Profesional 2018...', 'noticia0003.jpg', 'http://www.ccpmadrededios.org.pe/', '2018-06-08 15:55:41');

-- --------------------------------------------------------

--
-- Table structure for table `h_ponente`
--

CREATE TABLE `h_ponente` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `especialidad` varchar(200) NOT NULL,
  `ponente` varchar(50) NOT NULL,
  `id_evento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_ponente`
--

INSERT INTO `h_ponente` (`id`, `nombre`, `especialidad`, `ponente`, `id_evento`) VALUES
(1, 'Bruno Portuguez Nolasco', 'Pintor, Docente', 'ponente0001.PNG', 1),
(2, 'SR. GIANCARLO RATTO', 'Maestria en Administración de Empresas', 'ponente0002.PNG', 2),
(3, 'CPC. AUREA MEZA BARRON', 'Especialista, Contadora', 'ponente0003.png', 3),
(4, 'Lic. Mildrete Gutierrez', 'Socia principal de global oportunitis', 'ponente1002.PNG', 2);

-- --------------------------------------------------------

--
-- Table structure for table `h_slide`
--

CREATE TABLE `h_slide` (
  `id` int(11) NOT NULL,
  `slide` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_slide`
--

INSERT INTO `h_slide` (`id`, `slide`) VALUES
(9, 'slide0009.png'),
(10, 'slide0010.png');

-- --------------------------------------------------------

--
-- Table structure for table `prueba`
--

CREATE TABLE `prueba` (
  `id` int(11) NOT NULL,
  `dedo` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `h_admin`
--
ALTER TABLE `h_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_administrador`
--
ALTER TABLE `h_administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_certificado`
--
ALTER TABLE `h_certificado`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_colegiado`
--
ALTER TABLE `h_colegiado`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_decano`
--
ALTER TABLE `h_decano`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_enlace`
--
ALTER TABLE `h_enlace`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_evento`
--
ALTER TABLE `h_evento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_galeria`
--
ALTER TABLE `h_galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_imagenes`
--
ALTER TABLE `h_imagenes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_informe`
--
ALTER TABLE `h_informe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_miembro`
--
ALTER TABLE `h_miembro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_normas_legales`
--
ALTER TABLE `h_normas_legales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_nosotros`
--
ALTER TABLE `h_nosotros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_noticia`
--
ALTER TABLE `h_noticia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_ponente`
--
ALTER TABLE `h_ponente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_slide`
--
ALTER TABLE `h_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prueba`
--
ALTER TABLE `prueba`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `h_admin`
--
ALTER TABLE `h_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `h_administrador`
--
ALTER TABLE `h_administrador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `h_decano`
--
ALTER TABLE `h_decano`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `h_enlace`
--
ALTER TABLE `h_enlace`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `h_evento`
--
ALTER TABLE `h_evento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `h_galeria`
--
ALTER TABLE `h_galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `h_imagenes`
--
ALTER TABLE `h_imagenes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `h_informe`
--
ALTER TABLE `h_informe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `h_miembro`
--
ALTER TABLE `h_miembro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `h_normas_legales`
--
ALTER TABLE `h_normas_legales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `h_nosotros`
--
ALTER TABLE `h_nosotros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `h_noticia`
--
ALTER TABLE `h_noticia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `h_ponente`
--
ALTER TABLE `h_ponente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `h_slide`
--
ALTER TABLE `h_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `prueba`
--
ALTER TABLE `prueba`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
