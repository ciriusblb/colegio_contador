# COLEGIO-DE-INGENIEROS-DEL-PERU-V1
esta es la carpeta que contiene todo el codigo para la sitio web
del colegio de ingenieros del peru


------------
copiar la base de datos 
------------
## la base de datos se encuentra en el archivo 'ingeniero_db.sql'

debe importarlo en el gestor de base de datos XAMPP 

------------
npm install 
------------
## Para iniciar el proyecto si es la primera vez que lo abre

debe ejecutar en la consola dirigida a la carpeta del proyecto 
el comando 'npm install'


------------
npm start 
------------
## Para iniciar el proyecto luego del NPM INSTALL

debe ejecutar en la consola dirigida a la carpeta del proyecto 
el comando 'npm start' y dirigirse al navegador y escribir la dirección 
'https://Localhost:8000'
