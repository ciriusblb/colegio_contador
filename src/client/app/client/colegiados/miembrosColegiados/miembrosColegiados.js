(function(){
	'use strict';
	angular.module('app.client.colegiados.miembrosColegiados')
		.controller('MiembrosColegiados',MiembrosColegiados);
    MiembrosColegiados.$inject = ['colegiadoService','servicios'];
	function MiembrosColegiados(colegiadoService,servicios){
		var vm = this;
		vm.datos=doData();
		vm.buscar=function(){
			colegiadoService.getColegiado(vm.datos).then(function(data){
				vm.colegiado=data;
				vm.datos=doData();
			})
		}
		vm.limpiar= function(){
			vm.colegiado=undefined;
		}
		function doData(){
			return {
				matricula:'',
				ap_Paterno:'',
				ap_Materno:'',
				nombre:''
			}
		}
	}	
}());