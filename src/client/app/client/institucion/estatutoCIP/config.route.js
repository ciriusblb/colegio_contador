(function(){
	'use strict';
	 angular.module('app.client.institucion.estatutoCIP')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.institucion.estatutoCIP',
		 		config : {
		 			url:'/Estatuto_CIP',
		 			templateUrl : 'app/client/institucion/estatutoCIP/estatutoCIP.html',
		 			controller : 'EstatutoCIP',
		 			controllerAs: 'vm',		 			 
		 			title : 'Estatuto del CIP Madre de Dios',
		 			settings : {
			 				nav : 5,
			 				content : 'Estatuto del CIP Madre de Dios'
		 			}

		 		}
		 	}
	 	];
	 }
}());