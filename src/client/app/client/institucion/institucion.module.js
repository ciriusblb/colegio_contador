(function(){
	'use strict';
	angular.module('app.client.institucion',[
		'app.client.institucion.normasLegales',
		'app.client.institucion.himnoMDD',
		'app.client.institucion.himnoCIP',
		'app.client.institucion.nosotros',
		'app.client.institucion.estatutoCIP',
		'app.client.institucion.galeria'
		]);
}());