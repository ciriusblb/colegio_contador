(function(){
	'use strict';
	 angular.module('app.client.servicios.buscarColegiado')
	 	.run(appRun);

	appRun.$inject = ['routehelper'];

	 function appRun(routehelper){

	 	routehelper.configureRoutes( getRoutes() );

	 }

	 function getRoutes(){
	 	return[
		 	{
		 		name: 'Client.servicios.buscarColegiado',
		 		config : {
		 			// url:'/Certificacion',
		 			// templateUrl : 'app/client/servicios/buscarColegiado/buscarColegiado.html',
		 			// controller : 'Certificacion',
		 			// controllerAs: 'vm',		 			 
		 			// title : 'Certificacion',
		 			go: 'https://cipvirtual.cip.org.pe/sicecolegiacionweb/externo/consultaCol/',
		 			settings : {
			 				nav : 4,
			 				content : 'Buscar Colegiado'
		 			}

		 		}
		 	}
	 	];
	 }
}());
// target="_blank" href="https://cipvirtual.cip.org.pe/sicecolegiacionweb/externo/consultaCol/"